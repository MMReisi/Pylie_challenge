import os
import pandas as pd

# Read and prepare input data ..........................................................................................
here_path = os.path.dirname(os.path.realpath(__file__))
data_file_name = 'data_pylie_forecast_contest_2019.csv'
data_file_path = os.path.join(here_path, data_file_name)

df = pd.read_csv(data_file_path, index_col=0)
df.index = pd.to_datetime(df.index)
# ......................................................................................................................


# Your code here .......................................................................................................
my_pylie_id = 'ali123'  # replace with your own pylie profile id


# ......................................................................................................................


# Write solution data ..................................................................................................
solution_file_name = my_pylie_id + '_data_pylie_forecast_contest_2019.csv'
solution_file_path = os.path.join(here_path, solution_file_name)
df.to_csv(solution_file_path)
# ......................................................................................................................
